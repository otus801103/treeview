//Compile whith cc `pkg-config --cflags gtk+-3.0` -o gtk gtk.c `pkg-config --libs gtk+-3.0` -Wall -Wextra -Wpedantic -std=c11

#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>

// Заполнение данных дерева
void fill_model(GtkTreeStore *store, GtkTreeIter *parent, const char *path) {
    DIR *dir = opendir(path);
    struct dirent *entry;
    GtkTreeIter iter;

    if (!dir) {
        return;
    }

    while ((entry = readdir(dir)) != NULL) {
        // Пропускаем каталоги "." и ".."
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        gtk_tree_store_append(store, &iter, parent);
        gtk_tree_store_set(store, &iter, 0, entry->d_name, -1);

        char *full_path = g_build_filename(path, entry->d_name, NULL);
        struct stat s;
        if (stat(full_path, &s) == 0) {
            if (S_ISDIR(s.st_mode)) {
                // Рекурсивный вызов для получения данных из директорий
                fill_model(store, &iter, full_path);
            }
        }
        g_free(full_path);
    }

    closedir(dir);
}

int main(int argc, char *argv[]) {
    gtk_init(&argc, &argv);

    // Создание окна
    GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), _("TreeView"));
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 400);

    // Создание модели дерева
    GtkTreeStore *store = gtk_tree_store_new(1, G_TYPE_STRING);

    // Заполнение моделью дерева из текущего каталога
    fill_model(store, NULL, ".");

    // Создание виджета просмотра дерева и установка модели
    GtkWidget *tree_view = gtk_tree_view_new_with_model(GTK_TREE_MODEL(store));
    GtkTreeViewColumn *column = gtk_tree_view_column_new_with_attributes(_("Files and Directories"),
                                                                         gtk_cell_renderer_text_new(),
                                                                         "text",
                                                                         0,
                                                                         NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view), column);
    gtk_container_add(GTK_CONTAINER(window), tree_view);

    // Сигнал для корректного завершения приложения при закрытии окна
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    // Отображение всех виджетов
    gtk_widget_show_all(window);

    // Запуск главного цикла GTK
    gtk_main();

    return 0;
}


